$( document ).ready(function() {

/*Attaching data attributes to all circles before using the popover*/

            $('circle').each(function () {
                $(this).attr({
                    "data-container": "body",
                    "data-toggle": "popover",
                    "data-placement": "top",
                    "data-html":true,
                    "data-trigger": "hover"
                });
            });

/*Setting individual messages for each popup.calling all circles using a single class for this demo*/

            $(".svg circle:nth-child(4)").each(function () {
                $(this).attr({
                    "data-title":"Ticket Raised",
                    "data-content":"New ticket #DC0007 was raised by <em>princess peach</em> <time>3 days ago</time>"
                });
            });

            $(".svg circle:nth-child(5)").each(function () {
                $(this).attr({
                    "data-title":"Staff Assigned",
                    "data-content":"<em>Mario</em> was assigned to this ticket <time>3 days ago<time>"
                });
            });


            $(".svg circle:nth-child(6)").each(function () {
                $(this).attr({
                    "data-title":"Status Changed",
                    "data-content":"<em>Mario</em> changed the status of the ticket to <em>'Open'</em> <time>2 days ago</time>"
                });
            });


            $(".svg circle:nth-child(7)").each(function () {
                $(this).attr({
                    "data-title":"Priority Changed",
                    "data-content":"<em>Mario</em> raised the priority of the ticket to <em>'High'</em> <time>Yesterday</time>"
                });
            });


            $(".svg circle:nth-child(8)").each(function () {
                $(this).attr({
                    "data-title":"New Message",
                    "data-content":"<em>Princess</em> sent a new message <em>\"What is happening..\"</em> <time>Yesterday</time>"
                });
            });


            $(".svg circle:nth-child(9)").each(function () {
                $(this).attr({
                    "data-title":"Staff Assigned",
                    "data-content":"Ticket was assigned to <em>Luigi</em> <time>Yesterday</time>"
                });
            });


            $(".svg circle:nth-child(10)").each(function () {
                $(this).attr({
                    "data-title":"Level Complete",
                    "data-content":"Ticket was <em>closed</em> by <em>Luigi</em> <time>2 Hours ago</time>"
                });
            });

/*Calling bootstrap popover. Depends on bootstrap css and js*/

            $('circle').popover();

/*Disabling pointer events on the icons so that the circles don't lose hover state*/

            $('image').css("pointer-events", "none");


/*Hiding and showing the timeline while animating the container*/

            $('.show').click(function() {
                    var container = $(this).parent().parent().siblings();
                    container.find('.timeline-container').fadeToggle("fast");

                    if (container.hasClass('big')) {
                            container.animate({height:48},200).removeClass('big');
                    } else {
                    container.animate({height:128},200).addClass('big');
                    }

                    $(this).toggleClass("showing");

                    if ($(this).hasClass("showing")) {
                        $(this).html("Hide Timeline");
                            }
                        else {
                        $(this).html("Show Timeline");
            }
            });

        });
