
/*Initiating seven SVG elements to be repeated across the seven tickets*/

var s = [Snap("#svg1"),Snap("#svg2"),Snap("#svg3"),Snap("#svg4"),Snap("#svg5"),Snap("#svg6"),Snap("#svg7")];

/*just a big for loop to repeat everything for each ticket. SVG ID needs to be unique, so cannot duplicate the timelines by repeating the markup.*/

            for(x=0;x<s.length;x++){
            var l = s[x].line(20, 25, 610, 25);
            l.attr({
                stroke: "#000",
                strokeWidth: 2
            });

/*Drawing the circles*/

            var bigCircle = [];

            for(i=22,j=0;i<=630;i+=100,j++){
              bigCircle[j] = s[x].circle(i,25,12);
              bigCircle[j].attr({
                stroke:"#000",
                strokeWidth: 2
                });
            };

/*Filling the circles with colours*/

            var colors = ["#ff8133","#F8E82E","#00b5d9","#ff5454","#02d174","#F8E82E","grey"]

            for(index=0;index<colors.length;index++){
                    bigCircle[index].attr({
                        fill: colors[index]
                    });
            }

/*Hover animation on the circles*/

            for(i=0;i<bigCircle.length;i++){
                bigCircle[i].hover(
                    function() {
                        this.animate({r:20},400,mina.elastic);},
                    function() {
                        this.animate({r:12},400,mina.elastic);
                    });
            };

/*Positioning icons inside the circles. Need to find a way to group circles with their respective icons.This works only for this demo.*/

            var img = s[x].image("icons/inbox_3.png", 14.5, 17,15,15);
            var img1 = s[x].image("icons/user.png", 116, 19);
            var img2 = s[x].image("icons/status.png", 217, 20);
            var img3 = s[x].image("icons/priority.png", 320, 18);
            var img4 = s[x].image("icons/communication.png", 417, 21);
            var img5 = s[x].image("icons/user.png", 516, 18);
            var img6 = s[x].image("icons/resolved.png", 618, 19);

            }
